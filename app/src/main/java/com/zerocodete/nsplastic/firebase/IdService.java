package com.zerocodete.nsplastic.firebase;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.AppLogger;
import com.zerocodete.nsplastic.app.AppUtils;

/**
 * Created by simikic on 12/16/17.
 */

public class IdService extends FirebaseInstanceIdService {

    private static final String TAG = IdService.class.getSimpleName();

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        AppLogger.d(TAG, "Refreshed token: " + refreshedToken);

        if (AppUtils.isUserLoggedIn()) {
            // TODO: 12/16/17 salji token u user

            String uid = FirebaseAuth.getInstance().getUid();
            FirebaseDatabase
                    .getInstance()
                    .getReference()
                    .child(AppConstants.TABLE_USER)
                    .child(uid)
                    .child("deviceToken")
                    .setValue(refreshedToken);

        }
    }
}
