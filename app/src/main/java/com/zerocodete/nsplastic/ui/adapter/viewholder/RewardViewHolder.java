package com.zerocodete.nsplastic.ui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.GlideApp;
import com.zerocodete.nsplastic.callback.RewardClickListener;
import com.zerocodete.nsplastic.model.Reward;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 12/16/17.
 */

public class RewardViewHolder extends ChildViewHolder {

    @BindView(R.id.tv_title)
    TextView mTitle;
    @BindView(R.id.iv_bgnd)
    ImageView mSponsorBgnd;
    @BindView(R.id.tv_points)
    TextView mPoints;
    @BindView(R.id.tv_date)
    TextView mDate;

    private RewardClickListener mCallback;

    public RewardViewHolder(View itemView, RewardClickListener callback) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        mCallback = callback;
    }

    public void bind(final Reward reward) {
        GlideApp.with(mTitle.getContext())
                .load(reward.getSponsorImage())
                .error(R.drawable.ic_carddefault)
                .into(mSponsorBgnd);

        mTitle.setText(reward.getTitle());
        mPoints.setText(reward.getPoint().toString());
        mDate.setText(AppConstants.sDateFormat.format(new Date(reward.getExpirationDate()*1000)));
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onItemClick(reward);
            }
        });
    }
}
