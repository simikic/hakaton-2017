package com.zerocodete.nsplastic.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.clustering.ClusterManager;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.AppLogger;
import com.zerocodete.nsplastic.model.Container;
import com.zerocodete.nsplastic.model.Marker;
import com.zerocodete.nsplastic.ui.ContainerClusterRenderer;

import butterknife.ButterKnife;

/**
 * Created by simikic on 12/16/17.
 */

public class MapFragment extends Fragment implements OnMapReadyCallback {

    private static final String TAG = MapFragment.class.getSimpleName();
    private ClusterManager<Container> mClusterManager;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, rootView);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        return rootView;
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        AppLogger.e(TAG, " onMapReady ");

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(AppConstants.NOVI_SAD, 13);
        googleMap.animateCamera(cameraUpdate);

        mClusterManager = new ClusterManager<>(getActivity(), googleMap);
        final ContainerClusterRenderer renderer = new ContainerClusterRenderer(getContext(), googleMap, mClusterManager);
        mClusterManager.setRenderer(renderer);
        mClusterManager.setOnClusterItemClickListener(new ClusterManager.OnClusterItemClickListener<Container>() {
            @Override
            public boolean onClusterItemClick(Container container) {
                renderer.getMarker(container).showInfoWindow();
                return false;
            }
        });

        googleMap.getUiSettings().setMapToolbarEnabled(false);
        googleMap.getUiSettings().setRotateGesturesEnabled(false);
        googleMap.setOnMarkerClickListener(mClusterManager);
        googleMap.setOnCameraIdleListener(mClusterManager);
        googleMap.setOnInfoWindowClickListener(mClusterManager);

        FirebaseDatabase.getInstance().getReference().child(AppConstants.TABLE_MARKERS).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot tempSnap : dataSnapshot.getChildren()) {
                    Marker tempMarker = tempSnap.getValue(Marker.class);
                    mClusterManager.addItem(new Container(tempMarker));
                }
                mClusterManager.cluster();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
