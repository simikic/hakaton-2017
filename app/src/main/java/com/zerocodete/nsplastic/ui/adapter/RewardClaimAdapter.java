package com.zerocodete.nsplastic.ui.adapter;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;
import com.zerocodete.nsplastic.model.RewordClaim;
import com.zerocodete.nsplastic.ui.adapter.viewholder.RewardClaimViewHolder;

/**
 * Created by simikic on 12/17/17.
 */

public class RewardClaimAdapter extends FirebaseRecyclerAdapter<RewordClaim,
        RewardClaimViewHolder> {

    public RewardClaimAdapter(Class<RewordClaim> modelClass, int modelLayout, Class<RewardClaimViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);
    }

    @Override
    protected void populateViewHolder(RewardClaimViewHolder viewHolder, RewordClaim model, int position) {
        viewHolder.bind(model);
    }

}
