package com.zerocodete.nsplastic.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.callback.ItemClickListener;
import com.zerocodete.nsplastic.model.Feed;
import com.zerocodete.nsplastic.ui.activity.FeedDetailsActivity;
import com.zerocodete.nsplastic.ui.adapter.FeedsAdapter;
import com.zerocodete.nsplastic.ui.adapter.viewholder.FeedViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 8/26/17.
 */
public class FeedsFragment extends Fragment implements ItemClickListener {
    private static final String TAG = FeedsFragment.class.getSimpleName();

    @BindView(R.id.rv_feeds)
    RecyclerView mFeedsList;

    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseRecyclerAdapter<Feed, FeedViewHolder> mFirebaseAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_feeds, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, rootView);
//        .setIcon(R.mipmap.ic_launcher);

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mFirebaseAdapter = new FeedsAdapter(
                Feed.class,
                R.layout.li_feed,
                FeedViewHolder.class,
                mFirebaseDatabaseReference.child(AppConstants.TABLE_FEEDS),
                this);
        mFeedsList.setAdapter(mFirebaseAdapter);
        return rootView;
    }

    // TODO: 12/16/17 CHECK THIS
//    @Override
//    public void onPause() {
//        mFirebaseAdapter.stopListening();
//        super.onPause();
//    }
//
//    @Override
//    public void onResume() {
//        super.onResume();
//        mFirebaseAdapter.startListening();
//    }


    @Override
    public void onItemClicked(int position) {
        Feed feed = mFirebaseAdapter.getItem(position);
        Intent feedDetailsIntent = new Intent(getActivity(), FeedDetailsActivity.class);
        feedDetailsIntent.putExtra(AppConstants.ARG_FEED, feed);
        startActivity(feedDetailsIntent);
    }
}
