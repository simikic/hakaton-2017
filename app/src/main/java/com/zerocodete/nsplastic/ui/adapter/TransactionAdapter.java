package com.zerocodete.nsplastic.ui.adapter;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;
import com.zerocodete.nsplastic.model.Transaction;
import com.zerocodete.nsplastic.ui.adapter.viewholder.TransactionViewHolder;

/**
 * Created by simikic on 12/17/17.
 */

public class TransactionAdapter extends FirebaseRecyclerAdapter<Transaction,
        TransactionViewHolder> {

    public TransactionAdapter(Class<Transaction> modelClass, int modelLayout, Class<TransactionViewHolder> viewHolderClass, Query ref) {
        super(modelClass, modelLayout, viewHolderClass, ref);
    }


    @Override
    protected void populateViewHolder(TransactionViewHolder viewHolder, Transaction model, int position) {
        viewHolder.bind(model);
    }
}
