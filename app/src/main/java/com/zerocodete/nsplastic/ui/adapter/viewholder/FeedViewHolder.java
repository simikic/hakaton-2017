package com.zerocodete.nsplastic.ui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.GlideApp;
import com.zerocodete.nsplastic.callback.ItemClickListener;
import com.zerocodete.nsplastic.model.Feed;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 12/16/17.
 */

public class FeedViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.iv_avatar)
    ImageView mAvatar;
    @BindView(R.id.tv_title)
    TextView mTitle;
    @BindView(R.id.cl_background)
    View mItemBackground;

    public FeedViewHolder(View rootView) {
        super(rootView);
        ButterKnife.bind(this, rootView);
    }

    public void bind(Feed model, final ItemClickListener callback) {
        mTitle.setText(model.getTitle());

        int bgdRes;
        switch (model.getType()) {
            case 1:
                bgdRes = R.drawable.ic_cardgreen;
                break;
            case 2:
                bgdRes = R.drawable.ic_cardorange;
                break;
            default:
                bgdRes = R.drawable.ic_carddefault;
        }

        mItemBackground.setBackgroundResource(bgdRes);

        GlideApp.with(mAvatar.getContext())
                .load(model.getImage())
                .error(R.drawable.ic_def)
                .into(mAvatar);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (callback != null) {
                    callback.onItemClicked(getAdapterPosition());
                }
            }
        });
    }
}
