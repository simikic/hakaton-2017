package com.zerocodete.nsplastic.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.model.RewordClaim;
import com.zerocodete.nsplastic.ui.adapter.RewardClaimAdapter;
import com.zerocodete.nsplastic.ui.adapter.viewholder.RewardClaimViewHolder;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 12/17/17.
 */

public class RewardListFragment extends Fragment {
    @BindView(R.id.rv_transactions)
    RecyclerView mTransactions;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_transaction_list, container, false);
        ButterKnife.bind(this, rootView);

        DatabaseReference firebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        RewardClaimAdapter transactionAdapter = new RewardClaimAdapter(
                RewordClaim.class,
                R.layout.li_reward_claim,
                RewardClaimViewHolder.class,
                firebaseDatabaseReference
                        .child(AppConstants.TABLE_USER_REWARD)
                        .child(FirebaseAuth.getInstance().getUid()));
        mTransactions.setAdapter(transactionAdapter);
        return rootView;
    }
}
