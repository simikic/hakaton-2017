package com.zerocodete.nsplastic.ui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.model.Transaction;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 12/17/17.
 */

public class TransactionViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_points)
    TextView mPoints;
    @BindView(R.id.tv_date)
    TextView mDate;

    public TransactionViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(Transaction model) {
        mPoints.setText(model.getPoint() + " (" + model.getWeight() + "kg)");
        mDate.setText(AppConstants.sDateFormat.format(new Date(model.getDate()*1000)));
    }
}
