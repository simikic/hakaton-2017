package com.zerocodete.nsplastic.ui;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.zerocodete.nsplastic.app.AppUtils;
import com.zerocodete.nsplastic.model.Container;
import com.zerocodete.nsplastic.model.Marker;

/**
 * Created by simikic on 12/16/17.
 */

public class ContainerClusterRenderer extends DefaultClusterRenderer<Container> {

    public ContainerClusterRenderer(Context context, GoogleMap map, ClusterManager<Container> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(Container item, MarkerOptions markerOptions) {
        super.onBeforeClusterItemRendered(item, markerOptions);
        Marker marker = item.getMarker();
        markerOptions
                .icon(AppUtils.getDescriptorById(marker.getType()))
                .title(marker.getDescription())
                .snippet("");
    }
}