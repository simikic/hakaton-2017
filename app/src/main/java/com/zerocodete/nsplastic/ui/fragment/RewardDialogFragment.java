package com.zerocodete.nsplastic.ui.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.AppUtils;
import com.zerocodete.nsplastic.app.GlideApp;
import com.zerocodete.nsplastic.model.Reward;
import com.zerocodete.nsplastic.model.RewordClaim;
import com.zerocodete.nsplastic.model.User;
import com.zerocodete.nsplastic.ui.activity.LoginActivity;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rade on 2/16/2016.
 */
public class RewardDialogFragment extends DialogFragment {

    private static final String TAG = RewardDialogFragment.class.getSimpleName();

    @BindView(R.id.tv_description)
    TextView mDescription;
    @BindView(R.id.iv_bgnd)
    ImageView mSponsorBgnd;
    @BindView(R.id.tv_points)
    TextView mPoints;
    @BindView(R.id.tv_date)
    TextView mDate;
    private DatabaseReference mDatabase;

    public static RewardDialogFragment newInstance(Reward reward) {

        RewardDialogFragment fragment = new RewardDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(AppConstants.ARG_REWARD, reward);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.d_f_reward, null);
        ButterKnife.bind(this, dialogView);
        final Reward reward = getArguments().getParcelable(AppConstants.ARG_REWARD);


        GlideApp.with(getContext())
                .load(reward.getSponsorImage())
                .error(R.drawable.ic_carddefault)
                .into(mSponsorBgnd);

        mDescription.setText(reward.getDescription());
        mPoints.setText(reward.getPoint().toString());
        mDate.setText("Važi do " + AppConstants.sDateFormat.format(reward.getExpirationDate()));

        int posRes;
        if (AppUtils.isUserLoggedIn()) {
            posRes = R.string.action_buy;
        } else {
            posRes = R.string.action_sign_in;
        }
        mDatabase = FirebaseDatabase
                .getInstance()
                .getReference();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog dialog = builder
                .setTitle(reward.getTitle())
                .setView(dialogView)
                .setPositiveButton(posRes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        if (AppUtils.isUserLoggedIn()) {
                            final String uid = FirebaseAuth.getInstance().getUid();
                            mDatabase.child(AppConstants.TABLE_USER)
                                    .child(uid)
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            User user = dataSnapshot.getValue(User.class);
                                            if (user.getPoint() >= reward.getPoint()) {

                                                RewordClaim rewardClaim = new RewordClaim();
                                                rewardClaim.setDate(Integer.parseInt(System.currentTimeMillis() / 1000 + ""));
                                                rewardClaim.setRewardId(reward.getKey());
                                                rewardClaim.setStatus(0);
                                                rewardClaim.setTitle(reward.getTitle());
                                                rewardClaim.setPoints(reward.getPoint());
                                                rewardClaim.setSponsorImage(reward.getSponsorImage());

                                                String key = mDatabase
                                                        .child(AppConstants.TABLE_USER_REWARD)
                                                        .child(uid)
                                                        .push()
                                                        .getKey();

                                                Map<String, Object> rewardClaimMap = new HashMap<>();
                                                rewardClaimMap.put(key, rewardClaim);

                                                mDatabase
                                                        .child(AppConstants.TABLE_USER_REWARD)
                                                        .child(uid)
                                                        .updateChildren(rewardClaimMap);
                                            } else {
                                                if( getActivity() !=null ) {
                                                    Toast.makeText(getActivity(), R.string.buy_error, Toast.LENGTH_LONG).show();
                                                }
                                            }
                                        }

                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {

                                        }
                                    });
                        } else {
                            Intent loginActivity = new Intent(getActivity(), LoginActivity.class);
                            startActivity(loginActivity);
                            getActivity().finish();
                        }
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
        return dialog;
    }
}
