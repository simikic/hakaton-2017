package com.zerocodete.nsplastic.ui.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.model.RewordClaim;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 12/17/17.
 */

public class RewardClaimViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tv_points)
    TextView mPoints;
    @BindView(R.id.tv_title)
    TextView mTitle;

    public RewardClaimViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(RewordClaim model) {
        mPoints.setText(model.getPoints().toString());
        mTitle.setText(model.getTitle());
        if( model.getStatus() != 0 ) {
            itemView.setBackgroundResource(R.color.app_dirty_white);
        }
    }
}
