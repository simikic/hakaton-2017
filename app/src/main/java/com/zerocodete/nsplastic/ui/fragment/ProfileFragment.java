package com.zerocodete.nsplastic.ui.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.AppLogger;
import com.zerocodete.nsplastic.model.User;
import com.zerocodete.nsplastic.ui.activity.LoginActivity;
import com.zerocodete.nsplastic.ui.adapter.ProfileAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by simikic on 8/26/17.
 */
public class ProfileFragment extends Fragment {
    private static final String TAG = ProfileFragment.class.getSimpleName();


    @BindView(R.id.vp_tables)
    ViewPager mPager;
    @BindView(R.id.tv_user)
    TextView mUser;
    @BindView(R.id.tv_points)
    TextView mPoints;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.logout, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                AlertDialog dialog = new AlertDialog.Builder(getContext())
                        .setMessage(R.string.logout_question)
                        .setPositiveButton(R.string.logout, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                String uid = FirebaseAuth.getInstance().getUid();
                                FirebaseDatabase
                                        .getInstance()
                                        .getReference()
                                        .child(AppConstants.TABLE_USER)
                                        .child(uid)
                                        .child("deviceToken")
                                        .setValue("");
                                FirebaseAuth.getInstance().signOut();
                                Intent loginActivity = new Intent(getActivity(), LoginActivity.class);
                                startActivity(loginActivity);
                                getActivity().finish();
                            }
                        })
                        .setNegativeButton(R.string.cancel, null)
                        .create();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, rootView);
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child(AppConstants.TABLE_USER)
                .child(uid)
                .addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        mUser.setText(user.getName());
                        mPoints.setText(user.getPoint() + "");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

        mPager.setAdapter(new ProfileAdapter(getActivity().getSupportFragmentManager()));
        return rootView;
    }

    @OnClick(R.id.iv_qr_code)
    public void showQrCodeDialog() {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child(AppConstants.TABLE_USER)
                .child(uid)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        AppLogger.d(TAG, user.getQrToken().toString());
                        QrCodeDialogFragment.newInstance(user.getQrToken().toString()).show(getActivity().getSupportFragmentManager(), "");
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
}
