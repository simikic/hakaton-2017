package com.zerocodete.nsplastic.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.thoughtbot.expandablerecyclerview.listeners.GroupExpandCollapseListener;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.AppLogger;
import com.zerocodete.nsplastic.callback.RewardClickListener;
import com.zerocodete.nsplastic.model.Reward;
import com.zerocodete.nsplastic.model.RewardBundle;
import com.zerocodete.nsplastic.model.RewardGroup;
import com.zerocodete.nsplastic.ui.adapter.RewardsAdapter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 8/26/17.
 */
public class RewardFragment extends Fragment implements RewardClickListener {
    private static final String TAG = RewardFragment.class.getSimpleName();

    @BindView(R.id.rv_reward)
    RecyclerView mRewardList;

    private List<RewardBundle> mData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_reward, container, false);
        setHasOptionsMenu(true);
        ButterKnife.bind(this, rootView);
        setupData();
        return rootView;
    }

    private void setupData() {
        mData = new ArrayList<>();
        FirebaseDatabase
                .getInstance()
                .getReference()
                .child(AppConstants.TABLE_REWARD)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        HashMap<Integer, List<Reward>> mDataHash = new HashMap<>();
                        for (DataSnapshot tempSnap : dataSnapshot.getChildren()) {
                            Reward reward = tempSnap.getValue(Reward.class);
                            reward.setKey(tempSnap.getKey());
                            List<Reward> rewardList = mDataHash.get(reward.getGroup());
                            if (rewardList == null) {
                                rewardList = new ArrayList<>();
                                mDataHash.put(reward.getGroup(), rewardList);
                            }
                            rewardList.add(reward);
                        }

                        for (Integer tempKey : mDataHash.keySet()) {
                            if (tempKey >= 0 && tempKey < 4) {
                                mData.add(new RewardBundle(new RewardGroup(AppConstants.REWARDS[tempKey]), mDataHash.get(tempKey)));
                            } else {
                                mData.add(new RewardBundle(new RewardGroup(AppConstants.DUMMY_REWARD), mDataHash.get(tempKey)));
                            }
                        }

                        Collections.sort(mData, new Comparator<RewardBundle>() {
                            @Override
                            public int compare(RewardBundle rewardBundle, RewardBundle t1) {
                                return rewardBundle.getItems().get(0).getGroup().compareTo(t1.getItems().get(0).getGroup());
                            }
                        });
                        final RewardsAdapter adapter = new RewardsAdapter(getContext(), mData, RewardFragment.this);
                        int tempCount = 0;
                        for (int i = 0; i < mData.size(); i++) {
                            adapter.toggleGroup(tempCount + i);
                            tempCount += mData.get(i).getItems().size();
                        }
                        mRewardList.setAdapter(adapter);

                        adapter.setOnGroupExpandCollapseListener(new GroupExpandCollapseListener() {
                            @Override
                            public void onGroupExpanded(ExpandableGroup group) {
                                RewardBundle rb = (RewardBundle) group;
                                rb.setState(true);
                                adapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onGroupCollapsed(ExpandableGroup group) {
                                RewardBundle rb = (RewardBundle) group;
                                rb.setState(false);
                                adapter.notifyDataSetChanged();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    public void onItemClick(Reward reward) {

        AppLogger.e(TAG, reward.getTitle());

        RewardDialogFragment rdf = RewardDialogFragment.newInstance(reward);
        rdf.show(getActivity().getSupportFragmentManager(), "");
    }
}
