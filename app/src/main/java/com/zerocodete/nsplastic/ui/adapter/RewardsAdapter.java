package com.zerocodete.nsplastic.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.callback.RewardClickListener;
import com.zerocodete.nsplastic.model.Reward;
import com.zerocodete.nsplastic.model.RewardBundle;
import com.zerocodete.nsplastic.ui.adapter.viewholder.RewardGroupViewHolder;
import com.zerocodete.nsplastic.ui.adapter.viewholder.RewardViewHolder;

import java.util.List;

/**
 * Created by simikic on 12/16/17.
 */

public class RewardsAdapter extends ExpandableRecyclerViewAdapter<RewardGroupViewHolder, RewardViewHolder> {

    private RewardClickListener mCallback;
    private LayoutInflater mInflater = null;

    public RewardsAdapter(Context context, List<? extends ExpandableGroup> groups, RewardClickListener callback) {
        super(groups);
        if (context != null) {
            mInflater = LayoutInflater.from(context);
        }
        mCallback = callback;
    }

    @Override
    public RewardGroupViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.li_reward_group, parent, false);
        return new RewardGroupViewHolder(view);
    }

    @Override
    public RewardViewHolder onCreateChildViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.li_reward, parent, false);
        return new RewardViewHolder(view, mCallback);
    }

    @Override
    public void onBindChildViewHolder(RewardViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
        holder.bind((Reward) group.getItems().get(childIndex));
    }

    @Override
    public void onBindGroupViewHolder(RewardGroupViewHolder holder, int flatPosition, ExpandableGroup group) {
        holder.bind((RewardBundle) group);

    }
}
