package com.zerocodete.nsplastic.ui.fragment;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.AppUtils;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Rade on 2/16/2016.
 */
public class QrCodeDialogFragment extends DialogFragment {

    private static final String TAG = QrCodeDialogFragment.class.getSimpleName();

    @BindView(R.id.iv_qr_code)
    ImageView mQrCodeImage;
    @BindView(R.id.tv_qr_code)
    TextView mQrCode;

    public static QrCodeDialogFragment newInstance(String code) {

        QrCodeDialogFragment fragment = new QrCodeDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putString(AppConstants.ARG_CODE, code);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        View dialogView = getActivity().getLayoutInflater().inflate(R.layout.d_f_qr_code, null);
        ButterKnife.bind(this, dialogView);
        String code = getArguments().getString(AppConstants.ARG_CODE);

        mQrCode.setText(code);
        Bitmap bitmap = AppUtils.encodeAsBitmap(code);
        if (bitmap != null) {
            mQrCodeImage.setImageBitmap(bitmap);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        AlertDialog dialog = builder
                .setView(dialogView)
                .setNegativeButton(R.string.cancel, null)
                .create();
        return dialog;
    }
}
