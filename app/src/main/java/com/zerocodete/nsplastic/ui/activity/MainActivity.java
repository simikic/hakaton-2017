package com.zerocodete.nsplastic.ui.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.iid.FirebaseInstanceId;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.AppLogger;
import com.zerocodete.nsplastic.app.AppUtils;
import com.zerocodete.nsplastic.ui.fragment.FeedsFragment;
import com.zerocodete.nsplastic.ui.fragment.LoginFragment;
import com.zerocodete.nsplastic.ui.fragment.MapFragment;
import com.zerocodete.nsplastic.ui.fragment.ProfileFragment;
import com.zerocodete.nsplastic.ui.fragment.RewardFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    private String TAG = MainActivity.class.getSimpleName();

    @BindView(R.id.bnv_menu)
    BottomNavigationView mMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupBottomMenu();

        if (savedInstanceState == null) {
            Fragment fragment = new FeedsFragment();
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragment_container, fragment);
            ft.commitAllowingStateLoss();
        }

        if (AppUtils.isUserLoggedIn()) {
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            AppLogger.d(TAG, "Refreshed token: " + refreshedToken);

            if (AppUtils.isUserLoggedIn()) {
                // TODO: 12/16/17 salji token u user

                String uid = FirebaseAuth.getInstance().getUid();
                FirebaseDatabase
                        .getInstance()
                        .getReference()
                        .child(AppConstants.TABLE_USER)
                        .child(uid)
                        .child("deviceToken")
                        .setValue(refreshedToken);

            }
        }
    }

    private void setupBottomMenu() {
        AppUtils.disableShiftMode(mMenu);
        mMenu.setOnNavigationItemSelectedListener(this);
        mMenu.setOnNavigationItemReselectedListener(new BottomNavigationView.OnNavigationItemReselectedListener() {
            @Override
            public void onNavigationItemReselected(@NonNull MenuItem item) {
                AppLogger.w(TAG, "Reselect " + item.getTitle());
            }
        });
    }

    @Override
    public void onBackPressed() {
        AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                .setMessage(R.string.exit_question)
                .setPositiveButton(R.string.exit, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .setNegativeButton(R.string.cancel, null)
                .create();
        dialog.show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        AppLogger.d(TAG, "Selected " + item.getTitle());
        Fragment fragment;
        switch (item.getItemId()) {
            case R.id.nav_feeds:
                fragment = new FeedsFragment();
                break;
            case R.id.nav_map:
                fragment = new MapFragment();
                break;
            case R.id.nav_reword:
                fragment = new RewardFragment();
                break;
            default:
                if (AppUtils.isUserLoggedIn()) {
                    fragment = new ProfileFragment();
                } else {
                    fragment = new LoginFragment();
                }
                break;
        }

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment_container, fragment);
        ft.commitAllowingStateLoss();
        return true;
    }
}
