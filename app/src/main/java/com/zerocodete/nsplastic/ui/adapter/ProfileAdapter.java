package com.zerocodete.nsplastic.ui.adapter;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.zerocodete.nsplastic.ui.fragment.RewardListFragment;
import com.zerocodete.nsplastic.ui.fragment.TransactionListFragment;

/**
 * Created by simikic on 12/17/17.
 */

public class ProfileAdapter extends FragmentStatePagerAdapter {

    public ProfileAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new RewardListFragment();
            default:
                return new TransactionListFragment();
        }
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return "Nagrade";
            default:
                return "Transakcije";
        }
    }

    @Override
    public int getCount() {
        return 2;
    }
}
