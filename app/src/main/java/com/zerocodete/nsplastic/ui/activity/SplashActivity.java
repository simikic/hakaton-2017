package com.zerocodete.nsplastic.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;

import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.AppUtils;

public class SplashActivity extends BaseActivity {

    private Handler handler;
    private Runnable go2Main;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        go2Main = new Runnable() {
            @Override
            public void run() {
                // TODO: 12/16/17 user is logged in
                Intent mainActivity;
                if (AppUtils.isUserLoggedIn()) {
                    mainActivity = new Intent(SplashActivity.this, MainActivity.class);
                } else {
                    mainActivity = new Intent(SplashActivity.this, LoginActivity.class);
                }
                startActivity(mainActivity);
                finish();
            }
        };
    }

    @Override
    protected void onStart() {
        super.onStart();
        handler.postDelayed(go2Main, AppConstants.DELAY_SPLASH_SCREEN);
    }

    @Override
    protected void onStop() {
        super.onStop();
        handler.removeCallbacksAndMessages(null);
    }
}
