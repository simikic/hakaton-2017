package com.zerocodete.nsplastic.ui.activity;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

/**
 * Created by Core Station on 9/30/2017.
 */

public class BaseActivity extends AppCompatActivity {

    private Toast mToast;

    public void showToast(CharSequence text) {
        if (mToast != null) {
            mToast.cancel();
            mToast = null;
        }
        mToast = Toast.makeText(this, text, Toast.LENGTH_SHORT);
        mToast.show();
    }
}
