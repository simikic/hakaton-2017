package com.zerocodete.nsplastic.ui.adapter;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.Query;
import com.zerocodete.nsplastic.callback.ItemClickListener;
import com.zerocodete.nsplastic.model.Feed;
import com.zerocodete.nsplastic.ui.adapter.viewholder.FeedViewHolder;

/**
 * Created by simikic on 12/16/17.
 */

public class FeedsAdapter extends FirebaseRecyclerAdapter<Feed,
        FeedViewHolder> {

    private ItemClickListener mCallback;

    public FeedsAdapter(Class<Feed> modelClass, int modelLayout, Class<FeedViewHolder> viewHolderClass, Query ref, ItemClickListener callback) {
        super(modelClass, modelLayout, viewHolderClass, ref);

        mCallback = callback;
    }

    @Override
    protected void populateViewHolder(FeedViewHolder viewHolder, Feed model, int position) {
        viewHolder.bind(model, mCallback);

    }
}
