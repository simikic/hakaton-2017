package com.zerocodete.nsplastic.ui.adapter.viewholder;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;
import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.model.RewardBundle;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by simikic on 12/16/17.
 */

public class RewardGroupViewHolder extends GroupViewHolder {

    @BindView(R.id.tv_title)
    TextView mTitle;

    @BindView(R.id.iv_arrow)
    ImageView mArrow;

    public RewardGroupViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void bind(RewardBundle group) {
        mTitle.setText(group.getTitle());
        if (group.getState()) {
            mArrow.setImageResource(R.drawable.ic_arrow_up);
        } else {
            mArrow.setImageResource(R.drawable.ic_arrow_down);
        }
    }
}
