package com.zerocodete.nsplastic.ui.activity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.zerocodete.nsplastic.R;
import com.zerocodete.nsplastic.app.AppConstants;
import com.zerocodete.nsplastic.app.GlideApp;
import com.zerocodete.nsplastic.model.Feed;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FeedDetailsActivity extends BaseActivity {

    private Feed mFeed;

    @BindView(R.id.iv_avatar)
    ImageView mAvatar;


    @BindView(R.id.tv_title)
    TextView mTitle;

    @BindView(R.id.tv_description)
    TextView mDescription;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feed_details);

        ButterKnife.bind(this);
        mFeed = getIntent().getParcelableExtra(AppConstants.ARG_FEED);

        GlideApp.with(this)
                .load(mFeed.getImage())
                .error(R.drawable.ic_def)
                .into(mAvatar);
        mTitle.setText(mFeed.getTitle());

        mDescription.setText(mFeed.getContent());
    }
}
