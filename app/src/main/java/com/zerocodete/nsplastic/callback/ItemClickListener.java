package com.zerocodete.nsplastic.callback;

/**
 * Created by simikic on 12/17/17.
 */

public interface ItemClickListener {

    void onItemClicked(int position);
}
