package com.zerocodete.nsplastic.callback;

import com.zerocodete.nsplastic.model.Reward;

/**
 * Created by simikic on 12/17/17.
 */

public interface RewardClickListener {

    void onItemClick(Reward reward);
}
