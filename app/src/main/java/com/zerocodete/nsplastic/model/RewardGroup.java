package com.zerocodete.nsplastic.model;

/**
 * Created by simikic on 12/16/17.
 */

public class RewardGroup {
    private boolean mExpanded;
    private String mTitle;

    public RewardGroup(String mTitle) {
        this.mTitle = mTitle;
        this.mExpanded = true;
    }

    public boolean isExpanded() {
        return mExpanded;
    }

    public void setExpanded(boolean expanded) {
        mExpanded = expanded;
    }

    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        mTitle = title;
    }
}
