package com.zerocodete.nsplastic.model;

import java.util.List;

/**
 * Created by simikic on 12/16/17.
 */

public class User {

    private List<String> deviceTokens = null;
    private String name;
    private Integer point;
    private Integer qrToken;
    private String rewardList;

    private Integer role;

    public List<String> getDeviceTokens() {
        return deviceTokens;
    }

    public void setDeviceTokens(List<String> deviceTokens) {
        this.deviceTokens = deviceTokens;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public Integer getQrToken() {
        return qrToken;
    }

    public void setQrToken(Integer qrToken) {
        this.qrToken = qrToken;
    }

    public String getRewardList() {
        return rewardList;
    }

    public void setRewardList(String rewardList) {
        this.rewardList = rewardList;
    }

    public Integer getRole() {
        return role;
    }

    public void setRole(Integer role) {
        this.role = role;
    }

}
