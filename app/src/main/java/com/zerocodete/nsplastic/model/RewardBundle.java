package com.zerocodete.nsplastic.model;

import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

/**
 * Created by simikic on 12/16/17.
 */

public class RewardBundle extends ExpandableGroup<Reward> {

    private RewardGroup mRewardGroup;

    public RewardBundle(RewardGroup rewardGroup, List<Reward> items) {
        super(rewardGroup.getTitle(), items);
        this.mRewardGroup = rewardGroup;
    }

    public boolean getState() {
        return mRewardGroup.isExpanded();
    }

    public void setState(boolean state) {
        mRewardGroup.setExpanded(state);
    }
}