package com.zerocodete.nsplastic.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by simikic on 12/16/17.
 */

public class Reward implements Parcelable {

    private String key;
    private Integer active;
    private String description;
    private Long expirationDate;
    private Integer group;
    private Integer id;
    private Integer operatorId;
    private String sponsorImage;
    private Integer point;
    private String title;

    public Integer getActive() {
        return active;
    }

    public void setActive(Integer active) {
        this.active = active;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Long expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Integer getGroup() {
        return group;
    }

    public void setGroup(Integer group) {
        this.group = group;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getSponsorImage() {
        return sponsorImage;
    }

    public void setSponsorImage(String sponsorImage) {
        this.sponsorImage = sponsorImage;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.active);
        dest.writeString(this.description);
        dest.writeValue(this.expirationDate);
        dest.writeValue(this.group);
        dest.writeValue(this.id);
        dest.writeValue(this.operatorId);
        dest.writeString(this.sponsorImage);
        dest.writeValue(this.point);
        dest.writeString(this.title);
    }

    public Reward() {
    }

    protected Reward(Parcel in) {
        this.active = (Integer) in.readValue(Integer.class.getClassLoader());
        this.description = in.readString();
        this.expirationDate = (Long) in.readValue(Long.class.getClassLoader());
        this.group = (Integer) in.readValue(Integer.class.getClassLoader());
        this.id = (Integer) in.readValue(Integer.class.getClassLoader());
        this.operatorId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.sponsorImage = in.readString();
        this.point = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
    }

    public static final Parcelable.Creator<Reward> CREATOR = new Parcelable.Creator<Reward>() {
        @Override
        public Reward createFromParcel(Parcel source) {
            return new Reward(source);
        }

        @Override
        public Reward[] newArray(int size) {
            return new Reward[size];
        }
    };

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}