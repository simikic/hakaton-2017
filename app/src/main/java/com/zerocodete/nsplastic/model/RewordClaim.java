package com.zerocodete.nsplastic.model;

/**
 * Created by simikic on 12/17/17.
 */

public class RewordClaim {

    private String rewardId;
    private Integer status;
    private Integer date;
    private String title;
    private String sponsorImage;
    private Integer points;

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSponsoredImage() {
        return sponsorImage;
    }

    public void setSponsorImage(String sponsoredImage) {
        this.sponsorImage = sponsoredImage;
    }

    public Integer getPoints() {
        return points;
    }

    public void setPoints(Integer points) {
        this.points = points;
    }
}
