package com.zerocodete.nsplastic.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by simikic on 12/16/17.
 */

public class Feed implements Parcelable {

    private String content;
    private Integer date;
    private String image;
    private Integer markerId;
    private Integer priority;
    private String title;
    private Integer type;
    private Integer visibility;

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getDate() {
        return date;
    }

    public void setDate(Integer date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getMarkerId() {
        return markerId;
    }

    public void setMarkerId(Integer markerId) {
        this.markerId = markerId;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.content);
        dest.writeValue(this.date);
        dest.writeString(this.image);
        dest.writeValue(this.markerId);
        dest.writeValue(this.priority);
        dest.writeString(this.title);
        dest.writeValue(this.type);
        dest.writeValue(this.visibility);
    }

    public Feed() {
    }

    protected Feed(Parcel in) {
        this.content = in.readString();
        this.date = (Integer) in.readValue(Integer.class.getClassLoader());
        this.image = in.readString();
        this.markerId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.priority = (Integer) in.readValue(Integer.class.getClassLoader());
        this.title = in.readString();
        this.type = (Integer) in.readValue(Integer.class.getClassLoader());
        this.visibility = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<Feed> CREATOR = new Parcelable.Creator<Feed>() {
        @Override
        public Feed createFromParcel(Parcel source) {
            return new Feed(source);
        }

        @Override
        public Feed[] newArray(int size) {
            return new Feed[size];
        }
    };
}