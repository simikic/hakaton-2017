package com.zerocodete.nsplastic.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by simikic on 12/16/17.
 */

public class Container implements ClusterItem {

    private static final String TAG = Container.class.getSimpleName();

    private LatLng mTickLocation;
    private Marker mMarker;

    public Container(Marker marker) {
        mMarker = marker;
        mTickLocation = new LatLng(marker.getLat(), marker.getLng());
    }

    @Override
    public LatLng getPosition() {
        return mTickLocation;
    }

    @Override
    public String getTitle() {
        // TODO: 12/16/17 think about this
        return "";
    }

    @Override
    public String getSnippet() {
        return mMarker.getDescription();
    }

    public Marker getMarker() {
        return mMarker;
    }
}
