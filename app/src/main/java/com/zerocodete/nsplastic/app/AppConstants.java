package com.zerocodete.nsplastic.app;

import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.Locale;

/**
 * Created by simikic on 8/26/17.
 */
public class AppConstants {

    public static final String TABLE_FEEDS = "feed";
    public static final String TABLE_MARKERS = "marker";
    public static final String TABLE_REWARD = "reward";
    public static final String TABLE_USER = "user";
    public static final String TABLE_USER_REWARD = "userRewards";
    public static final String TABLE_TRANSACTIONS = "transaction";


    public static final String DUMMY_REWARD = "Nagrada";

    public static final String ARG_CODE = "code";
    public static final String ARG_FEED = "feed";
    public static final String ARG_REWARD = "reward";


    public static String[] REWARDS = {
            "0 - 250",
            "250 - 500",
            "500 - 1000",
            "1000 + "};

    public static LatLng NOVI_SAD = new LatLng(45.2671352, 19.8335496);

//    public static final String API_KEY = "AIzaSyAW4I7oEoA0vHeB1VduBQpHiL_t6t-Fn-k";


    public static final String PREF_CURRENCY = "pref_currency";
    public static final String PREF_CHART = "pref_chart";
    public static final String PREF_APP_VER = "pref_ver";
    public static final String PREF_FTI_DONE = "pref_fti_done";
    public static final String PREF_TIME_FILTER = "pref_time_filter";


    public static final String DEFAULT_CURRENCY = "RSD";
    public static final int DEFAULT_TIME_FILTER = 2;
    public static final int ADD_EXPENSE_RQ = 100;

    public static final String ARG_CATEGORY = "arg_category";
    public static final String ARG_COUNT = "arg_count";
    public static final String ARG_EXPENSE = "arg_expense";
    public static final String ARG_EDIT_MODE = "arg_edit_mode";
    public static final String ARG_EXPENSE_WITH_CATEGORY = "arg_expense_with_category";
    public static final String ARG_CATEGORY_ID = "arg_category_id";

    public static final long DELAY_SPLASH_SCREEN = 1200;


    public static SimpleDateFormat sDateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
    //        public static SimpleDateFormat sDateFormat = new SimpleDateFormat("EEEE, d MMMM", Locale.getDefault());
    public static SimpleDateFormat sDateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault());
    //    public static SimpleDateFormat sSimpleDateFormat = new SimpleDateFormat("dd-MMM", Locale.getDefault());

    public static final String PLAY_STORE_BASE = "https://play.google.com/store/apps/details?id=";

}
