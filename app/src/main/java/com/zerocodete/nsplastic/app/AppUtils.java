package com.zerocodete.nsplastic.app;

import android.graphics.Bitmap;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;

import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.firebase.auth.FirebaseAuth;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.zerocodete.nsplastic.R;

import java.lang.reflect.Field;
import java.util.Date;
import java.util.EnumMap;
import java.util.Map;

/**
 * Created by simikic on 8/26/17.
 */

public class AppUtils {

    private static final String TAG = AppUtils.class.getSimpleName();

    public static String getDateStringFormated(Date date) {
        return AppConstants.sDateFormat.format(date);
    }


    public static void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            AppLogger.e("BNVHelper", "Unable to get shift mode field");
        } catch (IllegalAccessException e) {
            AppLogger.e("BNVHelper", "Unable to change value of shift mode");
        }
    }

    public static BitmapDescriptor getDescriptorById(int id) {
        switch (id) {
            case 0:
                return BitmapDescriptorFactory.fromResource(R.drawable.ic_pin);
            case 1:
                return BitmapDescriptorFactory.fromResource(R.drawable.ic_green_pin);
            default:
                return BitmapDescriptorFactory.fromResource(R.drawable.ic_red_pin);
        }
    }

    public static Bitmap encodeAsBitmap(String contents) {
        int WHITE = 0xFFFFFFFF;
        int BLACK = 0xFF000000;
        Map<EncodeHintType, Object> hints = null;
        String encoding = "UTF-8";
        if (encoding != null) {
            hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result = null;
        try {
            result = writer.encode(contents, BarcodeFormat.QR_CODE, 300, 300, hints);
        } catch (WriterException exception) {
            AppLogger.e(TAG, exception.toString());
            return null;
        }

        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        // All are 0, or black, by default
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    public static boolean isUserLoggedIn() {
        return (FirebaseAuth.getInstance().getCurrentUser() != null);
    }
}
